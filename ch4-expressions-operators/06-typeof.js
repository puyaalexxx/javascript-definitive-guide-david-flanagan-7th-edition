//undefined "undefined"
//null "object"
//true or false "boolean"
//any number or NaN "number"
//any BigInt "bigint"
//any string "string"
//any symbol "symbol"
//any function "function"
//any nonfunction object "object"

// If the value is a string, wrap it in quotes, otherwise, convert
(typeof value === "string") ? "'" + value + "'" : value.toString()

