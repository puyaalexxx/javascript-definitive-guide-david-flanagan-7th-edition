switch(n) {
    case 1: // Start here if n === 1
    // Execute code block #1.
    break; // Stop here
    case 2: // Start here if n === 2
    // Execute code block #2.
    break; // Stop here
    case 3: // Start here if n === 3
    // Execute code block #3.
    break; // Stop here
    default: // If all else fails...
    // Execute code block #4.
    break; // Stop here
}

function convert(x) {
    switch(typeof x) {
        case "number": // Convert the number to a hexadecimal integer
            return x.toString(16);
        case "string": // Return the string enclosed in quotes
            return '"' + x + '"';
        default: // Convert any other type in the usual way
            return String(x);
    }
}